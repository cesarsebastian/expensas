Expensas V 0.1
==============

Author : César Sebastián Carrazana.

How to install this project
---------------------------

  1. `git clone https://gitlab.com/cesarsebastian/expensas.git`
  1. `cd expensas`
  1. `composer install`
  1. Edit `expensas/app/config/parameters.yml` and configure
     credentials to acces a database for this demo.
  1. `php bin/console doctrine:database:create`
  1. `php bin/console doctrine:schema:create`
  1. `php bin/console doctrine:fixtures:load --append`
  1. `php bin/console assets:install --symlink`
  1. `php bin/console server:run`
  1. Browse `http://127.0.0.1:8000/admin/`


-----------------------------

Update BD

`php bin/console doctrine:schema:update --force`

Drop BD

`php bin/console doctrine:schema:drop --force`

-----------------------------

Configuracion de BD (parameter.yml)

`database_url: 'mysql://root:@127.0.0.1:3306/expensas?user=root&password=root'`