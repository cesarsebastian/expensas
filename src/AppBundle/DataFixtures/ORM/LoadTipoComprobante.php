<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\TipoComprobante;

class LoadTipoComprobante extends AbstractFixture implements OrderedFixtureInterface
{
    public function getOrder()
    {
        return 3;
    }

    public function load(ObjectManager $manager)
    {        
        $tipoComprobante = new TipoComprobante();
        $tipoComprobante->setDescripcion('Recibo');
        $tipoComprobante->setSigno("-1");
        $manager->persist($tipoComprobante);
        
        $tipoComprobante = new TipoComprobante();
        $tipoComprobante->setDescripcion('Factura');
        $tipoComprobante->setSigno('1');
        $manager->persist($tipoComprobante);        

        $manager->flush();
    }
}
