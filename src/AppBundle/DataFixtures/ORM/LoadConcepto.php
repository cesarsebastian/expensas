<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Concepto;

class LoadConcepto extends AbstractFixture implements OrderedFixtureInterface {

    public function getOrder() {
        return 1;
    }

    public function load(ObjectManager $manager) {
        
        $concepto = new Concepto();
        $concepto->setDescripcion('Pago mensual de expensas');
        $manager->persist($concepto);

        $concepto = new Concepto();
        $concepto->setDescripcion('Pago de deuda de expensas');
        $manager->persist($concepto);

        $manager->flush();
    }

}
