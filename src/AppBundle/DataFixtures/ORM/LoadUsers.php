<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\User;
use AppBundle\Entity\Administracion;

class LoadUsers extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    public function getOrder()
    {
        return 5;
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $encoder = $this->container->get('security.password_encoder');
        
        //ROLE_SUPER_ADMIN - desarrollo
        $user = new User();
        $user->setUsername('admin');
        $user->setEmail('admin@expensas.com.ar');
        $user->setRoles(array('ROLE_SUPER_ADMIN'));
        $user->setEnabled(true);        
        $user->setPassword($encoder->encodePassword($user, '1234'));
        $user->setCuit('1');
        $user->setDni('1');        
        $manager->persist($user);        
		
		///////////////////////////////////// ADMINISTRACIONES ////////////////////////////////////////////
		
		//ROLE_ADMIN - administracion PRADO		
		$prado = $manager->getRepository('AppBundle:Administracion')->findOneBy(array('email' => 'prado@gmail.com'));
		
		$user = new User();
        $user->setUsername('20999999992');
        $user->setEmail('prado-admin@gmail.com');
        $user->setRoles(array('ROLE_ADMIN'));
        $user->setEnabled(true);        
        $user->setPassword($encoder->encodePassword($user, '1234'));
        $user->setCuit('20999999992');
        $user->setDni('99999999');
        $user->setAdministracion($prado);        
        $manager->persist($user);
		
		//ROLE_ADMIN - administracion IKBA
		$ikba = $manager->getRepository('AppBundle:Administracion')->findOneBy(array('email' => 'ikba@gmail.com'));
		
		$user = new User();
        $user->setUsername('20888888882');
        $user->setEmail('ikba-admin@gmail.com');
        $user->setRoles(array('ROLE_ADMIN'));
        $user->setEnabled(true);        
        $user->setPassword($encoder->encodePassword($user, '1234'));
        $user->setCuit('20888888882');
        $user->setDni('88888888');
        $user->setAdministracion($ikba);        
        $manager->persist($user);
		
		///////////////////////////////////// CLIENTE ////////////////////////////////////////////
		
		//ROLE_CLIENT - cliente IKBA		
		$user = new User();
        $user->setUsername('20777777772');
        $user->setEmail('ikba-cliente@gmail.com');
        $user->setRoles(array('ROLE_CLIENT'));
        $user->setEnabled(true);        
        $user->setPassword($encoder->encodePassword($user, '1234'));
        $user->setCuit('20777777772');
        $user->setDni('77777777');
        $user->setAdministracion($ikba);        
        $manager->persist($user);
		
		//ROLE_CLIENT - cliente PRADO		
		$user = new User();
        $user->setUsername('20666666662');
        $user->setEmail('prado-cliente@gmail.com');
        $user->setRoles(array('ROLE_CLIENT'));
        $user->setEnabled(true);        
        $user->setPassword($encoder->encodePassword($user, '1234'));
        $user->setCuit('20666666662');
        $user->setDni('66666666');
        $user->setAdministracion($prado);        
        $manager->persist($user);	
		
		$manager->flush();
    }
}
