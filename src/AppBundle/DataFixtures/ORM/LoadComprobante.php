<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Comprobante;
use AppBundle\Entity\TipoComprobante;
use AppBundle\Entity\UnidadFuncional;
use AppBundle\Entity\Concepto;


class LoadComprobante extends AbstractFixture implements OrderedFixtureInterface
{
    public function getOrder()
    {
        return 7;
    }

    public function load(ObjectManager $manager)
    {
        //$recibo = $manager->getRepository('AppBundle:TipoComprobante')->findOneBy(array('descripcion' => 'Recibo'));
        $factura = $manager->getRepository('AppBundle:TipoComprobante')->findOneBy(array('descripcion' => 'Factura'));
		
		$concepto1 = $manager->getRepository('AppBundle:Concepto')->findOneBy(array('id' => 1));
        $concepto2 = $manager->getRepository('AppBundle:Concepto')->findOneBy(array('id' => 2));
		
		/////////////////////////////////// ---  PRADO  --- ///////////////////////////////////////
		
		///////////////////////////////// UNIDAD FUNCIONAL 1 ///////////////////////////////
		
        $unidadFuncional_prado_1 = $manager->getRepository('AppBundle:UnidadFuncional')->findOneBy(array('numero' =>'0001'));
        
        $comprobante = new Comprobante();
        $comprobante->setFecha(new \DateTime());
        $comprobante->setPrefijo('0000000F');
        $comprobante->setNumero('000000001');
        $comprobante->setImporte('13000');
        $comprobante->setConcepto($concepto1);
        $comprobante->setUnidad($unidadFuncional_prado_1);
        $comprobante->setTipoComprobante($factura);
        $manager->persist($comprobante);
        
        $comprobante = new Comprobante();
        $comprobante->setFecha(new \DateTime());
        $comprobante->setPrefijo('0000000F');
        $comprobante->setNumero('000000002');
        $comprobante->setImporte('13500');
        $comprobante->setConcepto($concepto2);
        $comprobante->setUnidad($unidadFuncional_prado_1);
        $comprobante->setTipoComprobante($factura);
		$comprobante->setIdpago(null);
        $manager->persist($comprobante);
		
		///////////////////////////////// UNIDAD FUNCIONAL 2 ///////////////////////////////
		
        $unidadFuncional_prado_2 = $manager->getRepository('AppBundle:UnidadFuncional')->findOneBy(array('numero' =>'0002'));
        
        $comprobante = new Comprobante();
        $comprobante->setFecha(new \DateTime());
        $comprobante->setPrefijo('0000000F');
        $comprobante->setNumero('000000001');
        $comprobante->setImporte('7000');
        $comprobante->setConcepto($concepto1);
        $comprobante->setUnidad($unidadFuncional_prado_2);
        $comprobante->setTipoComprobante($factura);
		$comprobante->setIdpago(null);
        $manager->persist($comprobante);
        
        $comprobante = new Comprobante();
        $comprobante->setFecha(new \DateTime());
        $comprobante->setPrefijo('0000000F');
        $comprobante->setNumero('000000002');
        $comprobante->setImporte('12500');
        $comprobante->setConcepto($concepto2);
        $comprobante->setUnidad($unidadFuncional_prado_2);
        $comprobante->setTipoComprobante($factura);
		$comprobante->setIdpago(null);		
        $manager->persist($comprobante);
		
		/////////////////////////////////// ---  IKBA  --- ///////////////////////////////////////
		
		///////////////////////////////// UNIDAD FUNCIONAL 1 ///////////////////////////////
		
        $unidadFuncional_ikba_1 = $manager->getRepository('AppBundle:UnidadFuncional')->findOneBy(array('numero' =>'0003'));
        
        $comprobante = new Comprobante();
        $comprobante->setFecha(new \DateTime());
        $comprobante->setPrefijo('0000000F');
        $comprobante->setNumero('000000001');
        $comprobante->setImporte('2000');
        $comprobante->setConcepto($concepto1);
        $comprobante->setUnidad($unidadFuncional_ikba_1);
        $comprobante->setTipoComprobante($factura);
		$comprobante->setIdpago(null);		
        $manager->persist($comprobante);
        
        $comprobante = new Comprobante();
        $comprobante->setFecha(new \DateTime());
        $comprobante->setPrefijo('0000000F');
        $comprobante->setNumero('000000002');
        $comprobante->setImporte('2800');
        $comprobante->setConcepto($concepto2);
        $comprobante->setUnidad($unidadFuncional_ikba_1);
        $comprobante->setTipoComprobante($factura);
		$comprobante->setIdpago(null);		
        $manager->persist($comprobante);
		
		///////////////////////////////// UNIDAD FUNCIONAL 2 ///////////////////////////////
		
        $unidadFuncional_ikba_2 = $manager->getRepository('AppBundle:UnidadFuncional')->findOneBy(array('numero' =>'0004'));
        
        $comprobante = new Comprobante();
        $comprobante->setFecha(new \DateTime());
        $comprobante->setPrefijo('0000000F');
        $comprobante->setNumero('000000001');
        $comprobante->setImporte('7000');
        $comprobante->setConcepto($concepto1);
        $comprobante->setUnidad($unidadFuncional_ikba_2);
        $comprobante->setTipoComprobante($factura);
		$comprobante->setIdpago(null);		
        $manager->persist($comprobante);
        
        $comprobante = new Comprobante();
        $comprobante->setFecha(new \DateTime());
        $comprobante->setPrefijo('0000000F');
        $comprobante->setNumero('000000002');
        $comprobante->setImporte('12500');
        $comprobante->setConcepto($concepto2);
        $comprobante->setUnidad($unidadFuncional_ikba_2);
        $comprobante->setTipoComprobante($factura);
		$comprobante->setIdpago(null);		
        $manager->persist($comprobante);

        ///mixto
        $unidadFuncional_ikba_m = $manager->getRepository('AppBundle:UnidadFuncional')->findOneBy(array('numero' =>'0005'));

        $comprobante = new Comprobante();
        $comprobante->setFecha(new \DateTime());
        $comprobante->setPrefijo('0000000F');
        $comprobante->setNumero('000000001');
        $comprobante->setImporte('5682.3');
        $comprobante->setConcepto($concepto1);
        $comprobante->setUnidad($unidadFuncional_ikba_m);
        $comprobante->setTipoComprobante($factura);
        $comprobante->setIdpago(null);
        $manager->persist($comprobante);

        $manager->flush();
    }
}
