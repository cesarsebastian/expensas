<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\MotivoAnulacion;

class LoadMotivoAnulacion extends AbstractFixture implements OrderedFixtureInterface {

    public function getOrder() {
        return 2;
    }

    public function load(ObjectManager $manager) {
        foreach (range(0, 2) as $i) {
            $motivoAnulacion = new MotivoAnulacion();
            $motivoAnulacion->setDescripcion('Expensa subida con error' . $i);
            $manager->persist($motivoAnulacion);
        }

        $manager->flush();
    }

}
