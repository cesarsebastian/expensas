<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\User;
use AppBundle\Entity\Administracion;
use AppBundle\Entity\UnidadFuncional;

class LoadUnidadFuncional extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    public function getOrder()
    {
        return 6;
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $encoder = $this->container->get('security.password_encoder');
        
        ///////////////////////ALTA UNIDADES FUNCIONALES PRADO////////////////////////////////
		
        $administracion_prado = $manager->getRepository('AppBundle:Administracion')->findOneBy(array('email' => 'prado@gmail.com'));        
        $cliente_prado = $manager->getRepository('AppBundle:User')->findOneBy(array('email' => 'prado-cliente@gmail.com'));        
        
        $unidadFuncional = new UnidadFuncional();
        $unidadFuncional->setNumero('0001');
        $unidadFuncional->setPropietario('Cliente prado');
		$unidadFuncional->setCuit($cliente_prado->getCuit());
        $unidadFuncional->setDni($cliente_prado->getDni());       
        $unidadFuncional->setCopropietario('-');
        $unidadFuncional->setEstado('Activo');
        $unidadFuncional->setSuperficie('123 mts.');
        $unidadFuncional->setTelefono('99999999');
        $unidadFuncional->setCelular('99999999');
        $unidadFuncional->setEmail($cliente_prado->getEmail());
        $unidadFuncional->setDireccion('Salta 9999');
        $unidadFuncional->setAdministracion($administracion_prado);
        $unidadFuncional->setUser($cliente_prado);        
        $manager->persist($unidadFuncional); 
		
		$unidadFuncional = new UnidadFuncional();
        $unidadFuncional->setNumero('0002');
        $unidadFuncional->setPropietario('Cliente prado');
		$unidadFuncional->setCuit($cliente_prado->getCuit());
        $unidadFuncional->setDni($cliente_prado->getDni());       
        $unidadFuncional->setCopropietario('-');
        $unidadFuncional->setEstado('Activo');
        $unidadFuncional->setSuperficie('234 mts.');
        $unidadFuncional->setTelefono('99999999');
        $unidadFuncional->setCelular('99999999');
        $unidadFuncional->setEmail($cliente_prado->getEmail());
        $unidadFuncional->setDireccion('Salta 8888');
        $unidadFuncional->setAdministracion($administracion_prado);
        $unidadFuncional->setUser($cliente_prado);        
        $manager->persist($unidadFuncional);
		
		
		///////////////////////ALTA UNIDADES FUNCIONALES IKBA////////////////////////////////
		
        $administracion_ikba = $manager->getRepository('AppBundle:Administracion')->findOneBy(array('email' => 'ikba@gmail.com'));        
        $cliente_ikba = $manager->getRepository('AppBundle:User')->findOneBy(array('email' => 'ikba-cliente@gmail.com'));        
        
        $unidadFuncional = new UnidadFuncional();
        $unidadFuncional->setNumero('0003');
        $unidadFuncional->setPropietario('Cliente ikba');
		$unidadFuncional->setCuit($cliente_ikba->getCuit());
        $unidadFuncional->setDni($cliente_ikba->getDni());     
        $unidadFuncional->setCopropietario('-');
        $unidadFuncional->setEstado('Activo');
        $unidadFuncional->setSuperficie('123 mts.');
        $unidadFuncional->setTelefono('99999999');
        $unidadFuncional->setCelular('99999999');
        $unidadFuncional->setEmail($cliente_ikba->getEmail());
        $unidadFuncional->setDireccion('Sarmiento 9999');
        $unidadFuncional->setAdministracion($administracion_ikba);
        $unidadFuncional->setUser($cliente_ikba);        
        $manager->persist($unidadFuncional); 
		
		$unidadFuncional = new UnidadFuncional();
        $unidadFuncional->setNumero('0004');
        $unidadFuncional->setPropietario('Cliente ikba');
		$unidadFuncional->setCuit($cliente_ikba->getCuit());
        $unidadFuncional->setDni($cliente_ikba->getDni()); 
        $unidadFuncional->setCopropietario('-');
        $unidadFuncional->setEstado('Activo');
        $unidadFuncional->setSuperficie('234 mts.');
        $unidadFuncional->setTelefono('99999999');
        $unidadFuncional->setCelular('99999999');
        $unidadFuncional->setEmail($cliente_ikba->getEmail());
        $unidadFuncional->setDireccion('Sarmiento 8888');
        $unidadFuncional->setAdministracion($administracion_ikba);
        $unidadFuncional->setUser($cliente_ikba);        
        $manager->persist($unidadFuncional);

        //Mixto
        $unidadFuncional = new UnidadFuncional();
        $unidadFuncional->setNumero('0005');
        $unidadFuncional->setPropietario('Multiple admin');
        $unidadFuncional->setCuit($cliente_prado->getCuit());
        $unidadFuncional->setDni($cliente_prado->getDni());
        $unidadFuncional->setCopropietario('Multiple admin');
        $unidadFuncional->setEstado('Activo');
        $unidadFuncional->setSuperficie('724 mts.');
        $unidadFuncional->setTelefono('99999999');
        $unidadFuncional->setCelular('99999999');
        $unidadFuncional->setEmail($cliente_prado->getEmail());
        $unidadFuncional->setDireccion('Sarmiento 8888');
        $unidadFuncional->setAdministracion($administracion_ikba);
        $unidadFuncional->setUser($cliente_prado);
        $manager->persist($unidadFuncional);

        
        $manager->flush();
    }
}
