<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\Administracion;

class LoadAdministracion extends AbstractFixture implements OrderedFixtureInterface
{
    public function getOrder()
    {
        return 4;
    }
    
    public function load(ObjectManager $manager)
    {      
        $administracion = new Administracion();
        $administracion->setRazonSocial('prado');
        $administracion->setEmail('prado@gmail.com');
        $administracion->setDomicilio('direccion de prado');
        $administracion->setResponsable('responsable prado');
        $administracion->setLocalidadId(1);
        $administracion->setActivo(1);
        $administracion->setClientId('4830067769892674');
        $administracion->setClientSecret('6ZxjPZLVQIbJOZCaIT5JRFvvWtAiDFQb');
        $manager->persist($administracion);
        		
		$administracion = new Administracion();
        $administracion->setRazonSocial('ikba');
        $administracion->setEmail('ikba@gmail.com');
        $administracion->setDomicilio('direccion de ikba');
        $administracion->setResponsable('responsable ikba');
        $administracion->setLocalidadId(1);
        $administracion->setActivo(1);
        $administracion->setClientId('4830067769892674');
        $administracion->setClientSecret('6ZxjPZLVQIbJOZCaIT5JRFvvWtAiDFQb');
        $manager->persist($administracion);
		
        $manager->flush();
    }
}
