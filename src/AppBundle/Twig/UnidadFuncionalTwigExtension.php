<?php

/*
 * This file is part of the EasyAdminBundle.
 *
 * (c) Javier Eguiluz <javier.eguiluz@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Twig;

use Doctrine\ORM\Mapping\ClassMetadata;
use JavierEguiluz\Bundle\EasyAdminBundle\Configuration\ConfigManager;
use Symfony\Component\PropertyAccess\PropertyAccessor;

use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Defines the filters and functions used to render the bundle's templates.
 *
 * @author Javier Eguiluz <javier.eguiluz@gmail.com>
 */
class UnidadFuncionalTwigExtension extends \Twig_Extension
{
//    private $configManager;
//    private $propertyAccessor;
//    private $debug;
//
//    public function __construct(ConfigManager $configManager, PropertyAccessor $propertyAccessor, $debug = false)
//    {
//        $this->configManager = $configManager;
//        $this->propertyAccessor = $propertyAccessor;
//        $this->debug = $debug;
//    }
    
    protected $doctrine;
    // Retrieve doctrine from the constructor
    public function __construct(RegistryInterface $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('unidad_funcional_saldo', array($this, 'getSaldo')),
            
        );
    }
    
    public function getSaldo($name, $item, $metadata, $fecha){   
              
        $saldo = $item->getSaldo($fecha);

        return $saldo;
    }

    
    
    public function getName()
    {
        return 'unidad_extension';
    }
}
