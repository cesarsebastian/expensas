<?php

namespace AppBundle\Servicios;

use Doctrine\ORM\EntityManager as Manager;
use AppBundle\Entity\UnidadFuncional;
use AppBundle\Entity\Comprobante;
use AppBundle\Entity\MpCompras;
use AppBundle\Entity\TipoComprobante;
use AppBundle\Entity\Concepto;
use DateTime;

//Clase auxiliar para complementar funciones de la vista de unidades funcionales.
class clientHelper
{
    private $em;
    private $container;

    public function __construct($entityManager, $container)
    {
        $this->em        = $entityManager;
        $this->container = $container;      
    }
    

    //Agrupo las unidades funcionales por adminsitracion.
    public function agruparUfAdmin($ufs)
    {
        //Traigo la lista de cada administracion.
        $admins = $this->distinctAdmins($ufs);

        //Para cada administración, busco las ufs de cada una.
        for($i=0;$i<=count($admins)-1;$i++)
        {
            $admin = $admins[$i];

            //Recorro todas las ufs para buscar.
            foreach ($ufs as $uf)
            {
                //Si encuentro la que coincide la agrego.
                if ($admin['admin']->getId()==$uf->getAdministracion()->getId())
                {
                    //Sumo la deuda.
                    if ($uf->getSaldo()>0)
                    {
                        array_push($admin['unidades'],$uf);
                        $admin['saldo'] = $admin['saldo']+(float)$uf->getSaldo();
                    }
                }
            }

            $admins[$i] = $admin;
        }

        return $admins;
    }

    //Armo un array de las dif administraciones.
    private function distinctAdmins($ufs)
    {
        $admins = array();

        //Recorro todas las ufs para buscar si existe en la lista.
        foreach ($ufs as $uf)
        {
            if ($this->existInArray($admins,$uf->getAdministracion()->getId())==false)
                array_push($admins,array("admin"    => $uf->getAdministracion(),
                                         "unidades" => [],
                                         "saldo"    => 0));
        }

        return $admins;
    }

    //Existe la adminsitracion en el array.
    private function existInArray($lista,$id)
    {
        foreach ($lista as $item)
        {
            if ($item['admin']->getId()==$id)
                return true;
        }

        return false;
    }

    //Marcar los comprobantes con el id de mp_compras
    private function marcarComprobantes($lista,$idcompra)
    {
        //Recorro todos los comprobantes para marcar uno por uno cada pago.
        foreach ($lista as $comprob)
        {
            //Traigo el comprobante.
            $comprobante = $this->em->getRepository('AppBundle:Comprobante')->find($comprob->comprobante->id);

            //Si lo encuentro le cargo el idpago.
            if ($comprobante!=null)
            {
                $comprobante->setIdpago($idcompra);
                $this->em->flush();
            }
        }
    }

    //Creo los comprobantes para cancelar la deuda.
    public function cancelarSaldoUf($id)
    {        
        //Traigo la info. sobre la compra.
        $mp  = $this->em->getRepository('AppBundle:MpCompras')->find($id);        

        if ($mp!=null)
        {
            $items = json_decode($mp->getItems());

            if ($items!=null)
            {
                //Cargo el idpago para cada comprobante.
                $this->marcarComprobantes($items,$id);

                //Para cada comprobante.
                foreach ($items as $item)
                {
                    //Traigo el concepto de pago.
                    $concepto = $this->em->getRepository('AppBundle:Concepto')->find(1);

                    //Traigo el tipo de comprobante, -1 recibo.
                    $tipoComp = $this->em->getRepository('AppBundle:TipoComprobante')->find(1);

                    //Traigo la unidad funcional.
                    $uf       = $this->em->getRepository('AppBundle:UnidadFuncional')->find($item->uf->id);

                    //Genero los comprobantes.
                    $comprob = new Comprobante();
                    $comprob->setFecha(new \DateTime());
                    $comprob->setConcepto($concepto);
                    $comprob->setTipoComprobante($tipoComp);
                    $comprob->setImporte($item->comprobante->saldo);
                    $comprob->setPrefijo('0000000R');
                    $comprob->setNumero('000000001');
                    $comprob->setDetalle(null);
                    $comprob->setComprobantecol(null);
                    $comprob->setMotivoAnulacion(null);
                    $comprob->setFchAnulacion(null);
                    $comprob->setUsuarioAnulacion(null);
                    $comprob->setUnidad($uf);
                    $comprob->setIdpago($id);
                    $comprob->setIdfactura($item->comprobante->id);
                    $this->em->persist($comprob);
                    $this->em->flush();
                }
            }
        }
    }
}
