<?php

namespace AppBundle\Servicios;

use AppBundle\Servicios\MP as Sdk;
use Symfony\Component\HttpKernel\Exception;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException; 

class MercadoPagoProcessor
{
    public  $ambiente;
    private $client;
    private $secret;
    public  $mp       = null;
    private $response = null;

    public function __construct()
    { 

    }

    //Hace un request, a la api de mp en base a una url.
    public function get($url)
    {
        return $this->mp->get($url);
    }

    //Seteo el ambiente.
    public function setAmbiente($val)
    {
        $this->ambiente = $val;               
    }

    //Hago el request de pago.
    public function crearPago($data)
    {
        //Desactivo el modo de sandbox, si hiciese falta.
        if ($this->ambiente=='P')
            $this->mp->sandbox_mode(false);

        if ($this->ambiente=='T')
            $this->mp->sandbox_mode(true);        

        //Traigo la url del formulario.
        $pref = $this->mp->create_preference($data);
        
        //Traigo la url.
        $url  = $this->urlFromPago($pref);

        //Guardo la respuesta.
        $this->response = $pref;

        return $url;
    }

    //Seteo las credenciales.
    public function setCredenciales($client,$key)   
    {
        $this->client = $client;
        $this->secret = $key;
        
        //Libero memoria.
        unset($this->mp);
        
        //Vuelvo a crear.
        $this->mp  = new MP($this->client,$this->secret);
    }

    //Traigo la url en base al pago.
    public function urlFromPago($obj)
    {
        if ($this->ambiente=='T')
            return $obj['response']['sandbox_init_point'];
            
        if ($this->ambiente=='P')       
            return $obj['response']['init_point'];
    }

    //Traigo info de un pedido realizado.
    public function getDataPreference($id)
    {
        //Traigo la respuesta, adapto el formato.
        return $this->mp->get_preference($id);
    }

    //Traugi el ambiente.
    public function getAmbiente()
    {
        return $this->ambiente;
    }

    //Obtengo la respuesta.
    public function getResponse()
    {
        return $this->response;
    }
    
    //Formateo la respuesta de las preferencias.
    public function formatDataPreference($data)
    {
        //Si la respuesta es correcta.
        if ($data['status']=='200')
        {
          if (isset($data['response']))
          {
            //Datos de la compra.
            $info = array(
                    "idCliente"    => $data['response']['client_id'],
                    "refExterna"   => $data['response']['external_reference'],
                    "id"           => $data['response']['id'],
                    "urlFormu"     => $data['response']['init_point'],
                    "urlFormuTest" => $data['response']['sandbox_init_point'],                  
                    "urlsBack"     => $data['response']['back_urls'],
                    "items"        => $data['response']['items']
                      );
            
            return $info;
          }
        }
        
        return null;
    }
    
    //Obtengo la info de un merchant_order
    public function getMerchantOrder($orderId)
    {
        return $this->get("/merchant_orders/".$orderId);;
    }
    
    //Traigo info de un usuario de mercadopago.
    public function getUser($userId)
    {
        return $this->get("/users/".$userId); 
    }
    
    //Traigo info de un pago dentro de mercadopago.
    public function getPayment($payId)
    {
        return $this->mp->get_payment($payId);
    }           
    
    //Formateo la informacion del usuario.
    public function formatUser($data)
    {
        //Si hay datos.
        if ($data['status']=='200')
        {
          if (isset($data['response']))
          {
            //Armo la info.
            $info = array(
                    "id"      => $data['response']['id'],
                    "nick"    => $data['response']['nickname'],
                    "nombre"  => '',
                    "apellido"=> ''
                     );
                     
            //Si esta disponible el nombre y apellido.
            if (isset($data['response']['first_name'])&&isset($data['response']['last_name']))
            {
                $info['nombre'] = $data['response']['first_name'];
                $info['nombre'] = $data['response']['last_name'];               
            }
            
            return $info;           
          }
        }   
        
        return null;    
    }
    
    //Analiza si el objeto MerchantOrder cumple con el total del item.
    public function cumplePagoMerchantOrder($data)
    {
        if ($data!=null)
        {
            $pagado = 0;
            
            //Para cada pago.
            foreach($data['response']['payments'] as $pago)
            {
                //Si esta aprobado.
                if ($pago['status']=='approved')
                    $pagado = $pagado+$pago['transaction_amount'];
            }
            
            //Si cumple el pago.
            if ($pagado>=$data['response']['total_amount'])
                return true;
            else
                return false;
        }
        else
            return false;
    }
    
    //Formateo la respuesta del merchantOrder.
    public function formatMerchantOrder($data)  
    {
        //Si la respuesta es correcta.
        if ($data['status']=='200')
        {
          if (isset($data['response']))
          {
            //Info de la orden.
            $info = array(
                    "ordenId"  =>  $data['response']['id'],
                    "prefId"   =>  $data['response']['preference_id'],
                    "cliente"  =>  array(
                                "idcliente" => $data['response']['payer']['id'],
                                "email"     => $data['response']['payer']['email'],
                                "nombre"    => '',
                                "apellido"  => '',
                                "nick"      => ''                               
                                ),
                    "items"    => $data['response']['items'],
                    "external" => $data['response']['external_reference']
                     );
            
            //Si esta el id usuario, puedo obtener datos de el para complementar.
            if (isset($data['response']['payer']['id']))
            {
                //Traigo al usuario.
                $user = $this->formatUser($this->getUser($data['response']['payer']['id']));
                
                //Si hay datos del usuario, completo el array.
                if ($user!=null)
                {
                    $info['cliente']['nombre']   = $user['nombre'];
                    $info['cliente']['apellido'] = $user['apellido'];
                    $info['cliente']['nick']     = $user['nick'];                                       
                }
            }
            
            return $info;
          }
        }
        
        return null;
    }      
}
