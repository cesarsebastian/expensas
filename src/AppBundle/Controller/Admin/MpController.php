<?php

namespace AppBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Doctrine\ORM\EntityManager as Manager;
use AppBundle\Entity\UnidadFuncional;
use AppBundle\Entity\MpCompras;
use DateTime;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


class MpController extends Controller
{
    private $sendMail = false;

    /**
     * @Route("/wait/{id}/", name="mp_comprar_wait")
     */
    public function waitAction($id)
    {
        $em     = $this->getDoctrine()->getManager();        
        $mp     = $em->getRepository('AppBundle:MpCompras')->find($id);       

        if ($id!=null)
        {
            //Traigo la compra y actualizo.
            $em    = $this->getDoctrine()->getManager();
            $mp    = $em->getRepository('AppBundle:MpCompras')->find($id);

            if ($mp!=null)
            {
                $user = $em->getRepository('AppBundle:User')->find($mp->getIduser());

                //Envio un email al cliente con todos los datos.
                $this->enviarEmail($mp,$user,'wait');
            }
            else
            {
                echo 'Id compra no encontrado';
                exit;
            }
        }

        return $this->render('AppBundle:Dashboard:clientConfirm.html.twig', array('mpData' => $mp,
                                                                                  'stat'   => 'wait',
                                                                                  'items'  => json_decode($mp->getItems())));
    }

    /**
     * @Route("/error/{id}/", name="mp_comprar_error")
     */
    public function errorAction($id)
    {
        if ($id!=null)
        {
            //Hago un request para obtener info sobre el pago.
            $mpProc = $this->get('mercadopago.processor');

            //Traigo la compra y actualizo.
            $em    = $this->getDoctrine()->getManager();
            $mp    = $em->getRepository('AppBundle:MpCompras')->find($id);

            if ($mp!=null)
            {
                $user = $em->getRepository('AppBundle:User')->find($mp->getIduser());

                //Envio un email al cliente con todos los datos.
                $this->enviarEmail($mp,$user,'error');

                //Grabo el estado.
                $tmp   = array("payDate" => new \DateTime(),
                               "data"    => $_GET);

                $reply = json_encode($tmp);
                $mp->setEstado('PENDIENTE');
                $mp->setLogPago($reply);
                $em->flush();                
            }
            else
            {
                echo 'Id compra no encontrado';
                exit;
            }
        }

        return $this->render('AppBundle:Dashboard:clientConfirm.html.twig', array('mpData' => $mp,
                                                                                  'stat'   => 'error',
                                                                                  'items'  => json_decode($mp->getItems())));
    }

    /**
     * @Route("/ok/{id}/", name="mp_comprar_ok")
     */
    public function okAction($id)
    {
        //Reviso si el pago fue exitoso.
        if ($id!=null)
        {
            //Traigo la compra y actualizo.
            $em    = $this->getDoctrine()->getManager();
            $mp    = $em->getRepository('AppBundle:MpCompras')->find($id);

            //Si vino el estado y esta aprobado.
            if (isset($_GET['collection_status']))
            {
                if ($_GET['collection_status']=='approved')
                {
                    //Hago un request para obtener info sobre el pago.
                    $mpProc = $this->get('mercadopago.processor');

                    //Client helper.
                    $helper = $this->get('client.helper');                    

                    if ($mp!=null)
                    {
                        $user = $em->getRepository('AppBundle:User')->find($mp->getIduser());

                        //Envio un email al cliente con todos los datos.
                        $this->enviarEmail($mp,$user,'ok');

                        //Grabo el estado.
                        $tmp   = array("payDate" => new \DateTime(),
                                       "data"    => $_GET);

                        $reply = json_encode($tmp);
                        $mp->setEstado('APROBADO');
                        $mp->setLogPago($reply);
                        $em->flush();

                        //Genero los comprobantes de pago de la compra.
                        $helper->cancelarSaldoUf($id);

                        return $this->render('AppBundle:Dashboard:clientConfirm.html.twig', array('mpData'  => $mp,
                                                                                                  'stat'    => 'ok',
                                                                                                  'items'   => json_decode($mp->getItems())));
                    }
                    else
                    {                        
                        echo 'Id compra no encontrado';
                        exit;
                    }
                }
                else
                {
                    $tmp   = array("payDate" => new \DateTime(),
                                   "data"    => $_GET);

                    $reply = json_encode($tmp);
                    $mp->setEstado('ERROR');
                    $mp->setLogPago($reply);
                    $em->flush();

                    return $this->render('AppBundle:Dashboard:clientConfirm.html.twig', array('mpData' => $mp,
                                                                                              'stat'   => 'error'));
                }
            }
        }
    }

    /**
     * @Route("/{ufId}/", name="mp_comprar")
     */
    public function indexAction($ufId)
    {
        $em     = $this->getDoctrine()->getManager();

        //Traigo el usuario logeado.
        $userId = $this->container->get('security.token_storage')->getToken()->getUser()->getId();

        //Parseo los ids por el guion.
        $ids    = explode('-', $ufId);
        
        //Armo un array con las unidades funcionales que voy a pagar.
        $ufArray = array();

        foreach ($ids as $id)
        {
            //Reviso si es numerico.
            if (is_numeric($id))
            {
                //Traigo el comprobante en base a su id.
                $comprob  = $em->getRepository('AppBundle:Comprobante')->find($id);

                //Si encontre el objeto.
                if ($comprob!=null)
                {
                    if ($userId == $comprob->getUnidad()->getUser()->getId())
                        array_push($ufArray,$comprob);
                }
            }
        }

        //Si hay objetos.
        if (count($ufArray)>0)
        {
            //Traigo el objeto con la info del pago y la url.
            $pay = $this->pagarMp($ufArray);

            if ($pay!=null)
            {
                //Actualizo el idcompra seteando los datos obtenidos con la url.
                if (isset($pay['idcompra']))
                {                    
                    $em    = $this->getDoctrine()->getManager();
                    $mp    = $em->getRepository('AppBundle:MpCompras')->find($pay['idcompra']);
                    $mp->setLogUrl(json_encode($pay['response']));
                    $em->flush();                 
                }

                //Redirecciono al formulario de MP.
                header('Location: '.$pay['url']);
                exit;
            }
            else
            {
                echo 'Hubo un problema para obtener la url de mercadopago.';
                exit;
            }      
        }
        else
        {
            echo 'Acceso restringido';
            exit;
        }
    }

    //Funciones auxiliares.

    //Traigo el ambiente del parameters.yml
    private function getAmbiente()
    {
        return $this->getParameter('mp_ambiente');
    }

    //Genero un array para enviar a MP y obtener la url del formulario.
    private function pagarMp($lista)
    {
        //Tengo que generar la descripcion del pago.
        $descrip = "Facturas ";
        $saldo   = 0;
        $admin   = null;

        //Recorro todos los comrobantes-
        foreach ($lista as $comprob)
        {
            $descrip = $descrip." UF: ".$comprob->getUnidad()->getDireccion().' '.$comprob->getUnidad()->getNumero();
            $saldo   = $saldo+$comprob->getImporte();
            $admin   = $comprob->getUnidad()->getAdministracion();
        }

        //Grabo la info basica de la compra.
        $idcompra = $this->recordCompra($descrip,$saldo,$lista,$admin);

       //Urls para retorno.
        $urls = array(
                        "ok"        => $this->generateUrl('mp_comprar_ok',array('id'=>$idcompra),    UrlGeneratorInterface::ABSOLUTE_URL),
                        "error"     => $this->generateUrl('mp_comprar_error',array('id'=>$idcompra), UrlGeneratorInterface::ABSOLUTE_URL),
                        "pendiente" => $this->generateUrl('mp_comprar_wait',array('id'=>$idcompra),  UrlGeneratorInterface::ABSOLUTE_URL),
                      );

        //Armo el array con los datos de la compra.
        $data = array(
                        "items" => array(
                                          array(
                                                "title"       => $descrip,
                                                "currency_id" => "ARS",
                                                "category_id" => "Expensas",
                                                "quantity"    => 1,
                                                "unit_price"  => $saldo,
                                                "description" => "Pago de expensas",
                                                "picture_url" => "http://ips.com.ar/img/expensas.png"
                                                )
                                        ),
                        "back_urls" => array(
                                                "success" => $urls['ok'],
                                                "failure" => $urls['error'],
                                                "pending" => $urls['pendiente']
                                            ),
                        "notification_url"   => $this->generateUrl('mp_ipn',array('id'=>$idcompra),  UrlGeneratorInterface::ABSOLUTE_URL),
                        "external_reference" => "MP".$idcompra
                    );

        //Reviso si tengo datos validos como para hacer un request a MP.
        if ($admin!=null)
        {
            //Traigo el servicio que maneja MP.
            $mpProc = $this->get('mercadopago.processor');

            //Seteo las credenciales y ambiente en base a lo que esta guardado en la bd.
            $mpProc->setCredenciales($admin->getClientId(),$admin->getClientSecret());

            //Para pruebas.
            $mpProc->setAmbiente($this->getAmbiente());

            //Hago un request a MP para obtener la url.
            return array("url"      => $mpProc->crearPago($data),
                         "response" => $mpProc->getResponse(),
                         "items"    => $lista,
                         "idcompra" => $idcompra);
        }
        else
        {
            echo 'No hay credenciales de MP.';
            exit;
        }
    }

    //Armo un array para registrar el campo.
    private function arrayUf($ufObj)
    {        
        return array(
                        "title"   => "Pago de expensas",
                        "descrip" => "UF ".$ufObj->getDireccion().' '.$ufObj->getNumero(),
                        "monto"   => $ufObj->getSaldo()
                    );
    }

    //Grabo la info de la compra.
    private function recordCompra($descrip,$saldo,$lista,$admin)
    {
        $em     = $this->getDoctrine()->getManager();

        //Grabo en la bd la info de la compra.
        $compra = new MpCompras();
        $compra->setFecha(new \DateTime());
        $compra->setDescrip($descrip);
        $compra->setCosto($saldo);
        $compra->setEstado('PENDIENTE'); 
        $compra->setIduser($this->container->get('security.token_storage')->getToken()->getUser()->getId());              

        //Agrego las credenciaes.
        if ($admin!=null)
        {
            $creds = array("clientId"       => $admin->getClientId(),
                           "clientSecret"   => $admin->getClientSecret());

            $compra->setCredenciales(json_encode($creds));
        }

        //Itero en el detalle de items y armo un array con los ids de cada UF que se va a pagar.
        $items = array();
        foreach ($lista as $item)
        {
            //Guardo en la tabla de compras la estructura.
            array_push($items,array("comprobante" => array("id"    => $item->getId(),
                                                           "num"   => $item->getNumero(),
                                                           "saldo" => $item->getImporte(),
                                                           "fecha" => $item->getFecha()),
                                    "uf"          => array("id"         => $item->getUnidad()->getId(),
                                                           "descrip"    => "UF ".ucfirst($item->getUnidad()->getDireccion()).' '.$item->getUnidad()->getNumero(),
                                                           "saldo"      => $item->getUnidad()->getSaldo())));
        }

        //Grabo en un json el detalle de la compra.
        $compra->setItems(json_encode($items));

        $em->persist($compra);
        $em->flush();

        return $compra->getId();
    }

    //Enviar email.
    private function enviarEmail($orden,$user,$stat)
    {
        if ($this->sendMail)
        {
            //Fijo el asunto segun el estado.
            $asunto = '';
            
            if ($stat=='ok')
                $asunto = 'Compra realizada';

            if ($stat=='error')
                $asunto = 'Compra rechazada';

            if ($stat=='wait')
                $asunto = 'Compra pendiente';

            //Armo los heades.
            $headers  = "MIME-Version: 1.0\r\n"; 
            $headers .= "Content-type: text/html; charset=utf-8\r\n";
            $headers .= $this->renderView('AppBundle:Dashboard:clientConfirmMail.html.twig', array('mpData' => $orden,'stat'=>$stat,'items'=>json_decode($orden->getItems())));

            $send = mail("info@sitio.com",$asunto,$user->getEmail(),$headers);

            return $send;
        }
        else
            return null;
    }
}
