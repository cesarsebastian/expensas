<?php

namespace AppBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\UnidadFuncional;

class DashboardController extends Controller {

    /**
     * @Route("/", name="dashboard")
     */
    public function dashboardAction() {

        $authorization = $this->container->get('security.authorization_checker');

        if ($authorization->isGranted('ROLE_SUPER_ADMIN')) {
            return $this->redirectToRoute('easyadmin', array(
                        'entity' => 'User',
                        'action' => 'list'
                    ));
        }
        
        if ($authorization->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('dashboard_administrator');
            
        }
        
        if ($authorization->isGranted('ROLE_CLIENT')) {
            return $this->redirectToRoute('dashboard_client');
        }
    }

    /**
     * @Route("/nueva_unidad_funcional", name="new_uf")
     */
    public function newUfAction() {

        return $this->render('AppBundle:Dashboard:new-uf.html.twig');
    }
    
    /**
     * @Route("/administrator", name="dashboard_administrator")
     */
    public function dashboardAdministratorAction(){
        return $this->render('AppBundle:Dashboard:admin.html.twig');
    }
    
    /**
     * @Route("/client", name="dashboard_client")
     */
    public function dashboardClientAction()
    {        
        $em    = $this->getDoctrine()->getManager();
        $cuit  = $this->container->get('security.token_storage')->getToken()->getUser()->getCuit();
        $uFs   = $em->getRepository('AppBundle:UnidadFuncional')->findBy(array('cuit' => $cuit));
        
        //Agrupo los resultados por administración.
        $clientHelper = $this->get('client.helper');
        $uFs          = $clientHelper->agruparUfAdmin($uFs);

        return $this->render('AppBundle:Dashboard:client.html.twig', array('gruposAdmins' => $uFs));
    }

    /**
     * @Route("/uflist/{idadmin}/", name="uf_list")
     */
    public function ufListAction($idadmin)
    {        
        $em    = $this->getDoctrine()->getManager();
        $cuit  = $this->container->get('security.token_storage')->getToken()->getUser()->getCuit();
        $uFs   = $em->getRepository('AppBundle:UnidadFuncional')->findBy(array('cuit' => $cuit));
        
        //Agrupo los resultados por administración.
        $clientHelper = $this->get('client.helper');
        $uFs          = $clientHelper->agruparUfAdmin($uFs);
        $resu         = null;

        foreach ($uFs as $grupo)
        {
            if ($grupo['admin']->getId()==$idadmin)
                $resu = $grupo;
        }

        return $this->render('AppBundle:Dashboard:ufList.html.twig', array('grupo' => $resu,'idadmin'=>$idadmin));
    }    
}
