<?php

namespace AppBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Doctrine\ORM\EntityManager as Manager;
use AppBundle\Entity\UnidadFuncional;
use AppBundle\Entity\MpCompras;
use DateTime;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class MpIpnController extends Controller
{
    private $sendMail = false;

    /**
     * @Route("/{id}/", name="mp_ipn")
     */
    public function ipnAction($id)
    {
        if ($id!=null)
        {
            //Traigo la compra y actualizo.
            $em    = $this->getDoctrine()->getManager();
            $mp    = $em->getRepository('AppBundle:MpCompras')->find($id);

            if ($mp!=null)
            {
                $user  = $em->getRepository('AppBundle:User')->find($mp->getIduser());

                //Envio un email al cliente con todos los datos.
                $this->enviarEmail($mp,$user,'ok');

                //Grabo el estado.
                $tmp   = array("payDate" => new \DateTime(),
                               "data"    => $_GET);

                //Traigo el merchant_order y lo uso para revisar si el pago fue realizado.
                if (isset($_GET['merchant_order_id']))
                {
                    if ($_GET['merchant_order_id']!='null')
                    {
                        //Seteo credenciales y ambiente.
                        $mpProc = $this->get('mercadopago.processor');
                        $creds  = json_decode($mp->getCredenciales());
                        $mpProc->setCredenciales($creds->clientId,$creds->clientSecret);                        
                        $mpProc->setAmbiente($this->getAmbiente());

                        //Hago el request y traigo info sobre el pago.
                        $orderMp = $mpProc->getMerchantOrder($_GET['merchant_order_id']);

                        //Reviso si esta cerrado el pago.
                        if (isset($orderMp['response']['status']))
                        {
                            if ($orderMp['response']['status']=='closed')
                            {
                                //Client helper.
                                $helper = $this->get('client.helper');

                                //Genero los comprobantes de pago de la compra.
                                $helper->cancelarSaldoUf($id);
                                
                                //Cambio estado a aprobado si el pago fue recibido.
                                $reply = json_encode($tmp);
                                $mp->setEstado('APROBADO');
                                $mp->setLogPago($reply);
                                $em->flush(); 
                            }
                        }
                    }
                }

                //Retorno 200 ok, esta respuesta es obligatorio para evitar que el api de mp intente nuevamente hasta obtener codigo 200.
                http_response_code(200);
                exit;
            }
        }

        //Retorno ERROR.
        http_response_code(500);
        exit;
    }

    //Traigo el ambiente del parameters.yml
    private function getAmbiente()
    {
        return $this->getParameter('mp_ambiente');
    }

    //Enviar email.
    private function enviarEmail($orden,$user,$stat)
    {
        if ($this->sendMail)
        {
            //Fijo el asunto segun el estado.
            $asunto = '';
            if ($stat=='ok')
                $asunto = 'Compra realizada';

            if ($stat=='error')
                $asunto = 'Compra rechazada';

            if ($stat=='wait')
                $asunto = 'Compra pendiente';

            //Armo los heades.
            $headers  = "MIME-Version: 1.0\r\n"; 
            $headers .= "Content-type: text/html; charset=utf-8\r\n";
            $headers .= $this->renderView('AppBundle:Dashboard:clientConfirmMail.html.twig', array('mpData' => $orden,'stat'=>$stat,'items'=>json_decode($orden->getItems())));

            $send = mail("info@sitio.com",$asunto,$user->getEmail(),$headers);

            return $send;
        }
        else
            return null;
    }
}
