<?php

namespace AppBundle\Controller\Admin;

use JavierEguiluz\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;

/**
 * This is an example of how to use a custom controller for a backend entity.
 */
class AdministracionController extends BaseAdminController
{  
    protected function createListQueryBuilder($entityClass, $sortDirection, $sortField = null, $dqlFilter = NULL)
    {
        if ($this->isGranted('ROLE_SUPER_ADMIN')) 
        {
            return parent::createListQueryBuilder($entityClass, $sortDirection);
        }
        
        $administracion = $this->container->get('security.token_storage')->getToken()->getUser()->getAdministracion();
       
        $em = $this->getDoctrine()->getManagerForClass($entityClass);

        $queryBuilder = $em->createQueryBuilder()
            ->select('entity')
            ->from($entityClass, 'entity')            
            ->andWhere('entity.id = '.$administracion->getId());

        if(null !== $sortField) {
            $queryBuilder->orderBy('entity.'.$sortField, $sortDirection);
        }

        return $queryBuilder;
        
    }
}
