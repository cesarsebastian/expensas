<?php

namespace AppBundle\Controller\Admin;

use JavierEguiluz\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;
use JavierEguiluz\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\User;
/**
 * This is an example of how to use a custom controller for a backend entity.
 */
class UnidadFuncionalController extends BaseAdminController
{  
    protected function createListQueryBuilder($entityClass, $sortDirection, $sortField = null, $dqlFilter = NULL)
    {
        if ($this->isGranted('ROLE_SUPER_ADMIN')) 
        {
            return parent::createListQueryBuilder($entityClass, $sortDirection);
        }
        
        $administracion = $this->container->get('security.token_storage')->getToken()->getUser()->getAdministracion();		
		
        $em = $this->getDoctrine()->getManagerForClass($entityClass);

        $queryBuilder = $em->createQueryBuilder()
            ->select('entity')
            ->from($entityClass, 'entity')
            ->join('entity.administracion', 'administracion')
            //->join('entity.comprobantes', 'comprobante')
            ->andWhere('administracion.id = '.$administracion->getId());

        if(null !== $sortField) {
            $queryBuilder->orderBy('entity.'.$sortField, $sortDirection);
        }

        return $queryBuilder;        
    }
    
    protected function prePersistEntity($entity)
    {
		//Si el usuario existe entonces  asociamos la unidad funcional al usuario existente
		//Sino creamos un nuevo usuario y la unidad funcional asociada a él		
		
		$user = $this->em->getRepository('AppBundle:User')->findOneByCuit($entity->getCuit());
		
		if(!$user)
		{		
			$encoder = $this->container->get('security.password_encoder');		
			$user = new User();
			$user->setUsername($entity->getCuit());
			$user->setEmail($entity->getEmail());
			$user->setRoles(array('ROLE_CLIENT'));
			$user->setEnabled(true);        
			$user->setPassword($encoder->encodePassword($user, $entity->getCuit()));
			$user->setCuit($entity->getCuit());
			$user->setDni($entity->getDni());
			$user->setAdministracion($this->container->get('security.token_storage')->getToken()->getUser()->getAdministracion());
			
			$this->em->persist($user);	
		}	
		
        $entity->setUser($user);
        $entity->setAdministracion($this->container->get('security.token_storage')->getToken()->getUser()->getAdministracion());
		
		$this->em->flush();
    }
    
    
    
    /**
     * The method that is executed when the user performs a query on an entity.
     *
     * @return Response
     */
    protected function searchAction()
    {
        $this->dispatch(EasyAdminEvents::PRE_SEARCH);

        // if the search query is empty, redirect to the 'list' action
        if ('' === $this->request->query->get('query')) {
            $queryParameters = array_replace($this->request->query->all(), array('action' => 'list', 'query' => null));
            $queryParameters = array_filter($queryParameters);

            return $this->redirect($this->get('router')->generate('easyadmin', $queryParameters));
        }
        
        $this->request->getSession()->set('fecha', $this->request->query->get('fecha'));
        //$this->request->getSession()->set('fecha', '2016-12-31 23:59:00');
        
        $searchableFields = $this->entity['search']['fields'];
        $paginator = $this->findBy($this->entity['class'], $this->request->query->get('query'), $searchableFields, $this->request->query->get('page', 1), $this->config['list']['max_results'], $this->request->query->get('sortField'), $this->request->query->get('sortDirection'), $this->entity['search']['dql_filter']);
        $fields = $this->entity['list']['fields'];

        $this->dispatch(EasyAdminEvents::POST_SEARCH, array(
            'fields' => $fields,
            'paginator' => $paginator,
        ));

        return $this->render($this->entity['templates']['list'], array(
            'paginator' => $paginator,
            'fields' => $fields,
            'delete_form_template' => $this->createDeleteForm($this->entity['name'], '__id__')->createView(),
        ));
    }
    
//    public function saldoAction(){
//        $this->dispatch(EasyAdminEvents::PRE_LIST);
//
//        $fields = $this->entity['saldo']['fields'];        
//        
//        $paginator = $this->findAll($this->entity['class'], $this->request->query->get('page', 1), $this->config['list']['max_results'], $this->request->query->get('sortField'), $this->request->query->get('sortDirection'));
//
//        $this->dispatch(EasyAdminEvents::POST_LIST, array('paginator' => $paginator));
//        
//        //var_dump($fields);
//        //exit();
//
//        return $this->render($this->entity['templates']['list'], array(
//            'paginator' => $paginator,
//            'fields' => $fields,
//            'delete_form_template' => $this->createDeleteForm($this->entity['name'], '__id__')->createView(),
//        ));
//    }
   
}
