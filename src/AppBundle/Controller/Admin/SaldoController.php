<?php

namespace AppBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\UnidadFuncional;

class SaldoController extends Controller {

    /**
     * @Route("/", name="saldo")
     */
    public function saldoAction() {
        
         $em = $this->getDoctrine()->getManager();
         
        $authorization = $this->container->get('security.authorization_checker');

        if ($authorization->isGranted('ROLE_SUPER_ADMIN')) {
            $unidadesFuncionales = $em->getRepository('AppBundle:UnidadFuncional')->findAll();
        } else {
            $administracion = $this->container->get('security.token_storage')->getToken()->getUser()->getAdministracion();        
            $unidadesFuncionales = $em->getRepository('AppBundle:UnidadFuncional')->findBy(array('administracion' => $administracion->getId()));
        }
        
        $result = array();
        $i = 0;
        foreach($unidadesFuncionales as $uf){
            $result[$i]['numero'] = $uf->getNumero();  
            $result[$i]['propietario'] = $uf->getPropietario();
            $result[$i]['saldo'] = $uf->getSaldo();
            $i++;
        }
        
        return $this->render('AppBundle:Informe:saldo.html.twig', array(
            'unidadesFuncionales' => $unidadesFuncionales
        ));
        
              
//        $query = $repository->createQueryBuilder('u')
//                            ->leftJoin('u.comprobantes', 'c')
//                            ->leftJoin('u.administracion', 'a')
//                            ->leftJoin('c.tipoComprobante', 'tc')
//                            ->addSelect('SUM(c.importe * tc.signo) AS total_sum')
//                            ->where('a.id = id')
//                            ->setParameter('id', $administracion->getId())
//                            ->groupBy('u.id')
//                            ->orderBy('u.id', 'ASC')                
//                            ->getQuery();
//
//        $unidadesFuncionales = $query->getResult();
        
    }
    
}
