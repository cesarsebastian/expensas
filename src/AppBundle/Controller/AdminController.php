<?php

namespace AppBundle\Controller;

use JavierEguiluz\Bundle\EasyAdminBundle\Controller\AdminController as EasyAdminController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Entity\UnidadFuncional;

class AdminController extends EasyAdminController {

    /**
     * Show Page List page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listPageAction() {
        $this->dispatch(EasyAdminEvents::PRE_LIST);
    }
    
    /**
     * @Route("/", name="easyadmin")
     */
    public function indexAction(Request $request) {
        
        if (null === $request->query->get('entity')) {
             // define this route in any of your own controllers
             return $this->redirectToRoute('dashboard');
        }
        
        return parent::indexAction($request);
    }

    public function createNewUserEntity() {
        return $this->get('fos_user.user_manager')->createUser();
    }

    public function prePersistUserEntity($user) {
        $this->get('fos_user.user_manager')->updateUser($user, false);
    }

    public function preUpdateUserEntity($user) {
        $this->get('fos_user.user_manager')->updateUser($user, false);
    }
}
