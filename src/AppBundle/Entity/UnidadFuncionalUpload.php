<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * UnidadFuncionalUpload
 * 
 * @ORM\Table(name="unidades_funcionales_uploads")
 * @ORM\Entity
 * @Vich\Uploadable
 */
class UnidadFuncionalUpload {

    /**
     * Cantidad de registros.
     *
     * @var string
     * @ORM\Column(type="integer", name="cantidad")
     */
    private $cantidad;

    /**
     * The creation date of the UnidadFuncionalUpload.
     *
     * @var \DateTime
     * @ORM\Column(type="datetime", name="created_at")
     */
    private $createdAt = null;

    /**
     * @var \AppBundle\Entity\User
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     * })
     */
    private $user;
    
     /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $contract;

    /**
     * @Vich\UploadableField(mapping="unidades_funcionales_uploads_files", fileNameProperty="contract")
     * @var File
     */
    private $contractFile;
    
    /**
     * @var \AppBundle\Entity\Administracion
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Administracion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="administracion_id", referencedColumnName="id")
     * })
     */
    private $administracion;

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * Constructor of the UnidadFuncionalUpload class.
     * (Initialize some fields).
     */
    public function __construct()
    {   
        $this->createdAt = new \DateTime();        
    }    
    
    public function __toString()
    {
        return 'Upload #'.$this->getId();
    }

    /**
     * Set cantidad
     *
     * @param string $cantidad
     *
     * @return UnidadFuncionalUpload
     */
    public function setCantidad($cantidad) {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return string
     */
    public function getCantidad() {
        return $this->cantidad;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set usuario
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return UnidadFuncionalUpload
     */
    public function setUser(\AppBundle\Entity\User $user = null) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    /**
     * @return \DateTime
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }
    
     /**
     * @param File $contract
     */
    public function setContractFile(File $contract = null)
    {
        $this->contractFile = $contract;
    }
    /**
     * @return File
     */
    public function getContractFile()
    {
        return $this->contractFile;
    }
    /**
     * @param string $contract
     */
    public function setContract($contract)
    {
        $this->contract = $contract;
    }
    /**
     * @return string
     */
    public function getContract()
    {
        return $this->contract;
    }
    
    /**
     * Set administracion
     *
     * @param \AppBundle\Entity\Administracion $administracion
     *
     * @return UnidadFuncionalUpload
     */
    public function setAdministracion(\AppBundle\Entity\Administracion $administracion = null)
    {
        $this->administracion = $administracion;

        return $this;
    }

    /**
     * Get administracion
     *
     * @return \AppBundle\Entity\Administracion
     */
    public function getAdministracion()
    {
        return $this->administracion;
    }

}
