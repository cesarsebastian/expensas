<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="administraciones") 
 * @ORM\Entity
 */
class Administracion
{   
    /**
     * Razon social.
     *
     * @var string
     * @ORM\Column(type="string", name="razon_social")
     */    
    private $razonSocial;

    /**
     * Email.
     *
     * @var string
     * @ORM\Column(type="string", name="email")
     */
    private $email;

   /**
     * Domicilio.
     *
     * @var string
     * @ORM\Column(type="string", name="domicilio")
     */
    private $domicilio;

    /**
     * Responsable.
     *
     * @var string
     * @ORM\Column(type="string", name="responsable")
     */
    private $responsable;

    /**
     * Localidad que luego hara ref a otra entidad.
     *
     * @var integer
     * @ORM\Column(type="integer", name="localidad_id")
     */   
    private $localidadId;
    
    /**
     * Activo
     *
     * @var bool
     * @ORM\Column(type="boolean", name="activo")
     */
    private $activo;

    /**
     * Fecha alta
     *
     * @var \DateTime
     * @ORM\Column(type="datetime", name="fch_alta")
     */
    private $fchAlta;

    /**
     * Fecha modificacion
     *
     * @var \DateTime
     * @ORM\Column(type="datetime", name="fch_modif", nullable=true)
     */
    private $fchModif;

    /**
     * Usuario de ultima modificacion
     *
     * @var string
     * @ORM\Column(type="string", name="usuario_modif", nullable=true)
     */   
    private $usuarioModif;

    /**
     * Usuario de alta
     *
     * @var string
     * @ORM\Column(type="string", name="usuario_alta", nullable=true)
     */   
    private $usuarioAlta;

    /**
     * Cliente
     *
     * @var string
     * @ORM\Column(type="string", name="client_id")
     */   
    private $clientId;

    /**
     * Cliente
     *
     * @var string
     * @ORM\Column(type="string", name="client_secret")
     */   
    private $clientSecret;

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;    
    
    /**
     * Users that have been Administraciones.
     *
     * @var Users[]
     * @ORM\OneToMany(targetEntity="User", mappedBy="administracion", cascade={"remove"})
     */
    protected $users;
    
    /**
     * Puntos de ventas de la administracion.
     *      
     * @var puntosVentas[]
     * @ORM\OneToMany(targetEntity="PuntoVenta", mappedBy="administracion")  
     */    
    protected $puntosVentas;
    
    /**
     * Unidades funcionales.
     *      
     * @var unidadFuncional[]
     * @ORM\OneToMany(targetEntity="UnidadFuncional", mappedBy="administracion")  
     */    
    protected $unidadesFuncionales;
    
    
    /**
     * Constructor of the Administracion class.
     * (Initialize some fields).
     */
    public function __construct()
    {   
        $this->fchAlta = new \DateTime();
        $this->fchModif = new \DateTime();
        $this->users = new ArrayCollection();
        $this->puntosVentas = new ArrayCollection();
        $this->unidadesFuncionales = new ArrayCollection();
    }
    
    
    public function __toString()
    {
        return $this->getRazonSocial();
    }
    
    /**
     * @param Users[] $users
     */
    public function setUsers($users)
    {
        $this->users = $users;
    }

    /**
     * @return Users[]
     */
    public function getUsers()
    {
        return $this->users;
    }
    
    public function addUser(\AppBundle\Entity\User $user)
    {   
        $this->user->add($user);
        $user->setList($this);
        
        return $this;
    }
    
    public function removeUser(\AppBundle\Entity\User $user)
    {
        $this->user->removeElement($user);
        return $this;
    }
    
    public function removeAllUser()
    {
        $this->user->clear();
    }
    
    /**
     * @param PuntoVenta[] $puntosVentas
     */
    public function setPuntosVentas($puntosVentas)
    {
        $this->puntosVentas = $puntosVentas;
    }    
        
    public function getPuntosVentas()
    {
        return $this->puntosVentas;
    }
    
    public function addPuntosVentas(\AppBundle\Entity\PuntoVenta $puntoVenta)
    {   
        $this->puntosVentas->add($puntoVenta);
        $puntoVenta->setAdministracion($this);
        
        return $this;
    }

    public function removePuntoVenta(\AppBundle\Entity\PuntoVenta $puntoVenta)
    {
        $this->puntosVentas->removeElement($puntoVenta);
        return $this;
    }
    
    public function removeAllPuntosVentas()
    {
        $this->puntosVentas->clear();
    }
    
    
    /**
     * @param UnidadFuncional[] $unidadesFuncionales
     */
    public function setUnidadesFuncionales($unidadesFuncionales)
    {
        $this->unidadesFuncionales = $unidadesFuncionales;
    }    
        
    public function getUnidadesFuncionales()
    {
        return $this->unidadesFuncionales;
    }
    
    public function addUnidadesFuncionales(\AppBundle\Entity\UnidadFuncional $unidadFuncional)
    {   
        $this->unidadesFuncionales->add($unidadFuncional);
        $unidadFuncional->setAdministracion($this);
        
        return $this;
    }

    public function removeUnidadFuncional(\AppBundle\Entity\UnidadFuncional $unidadFuncional)
    {
        $this->unidadesFuncionales->removeElement($unidadFuncional);
        return $this;
    }
    
    public function removeAllUnidadesFuncionales()
    {
        $this->unidadesFuncionales->clear();
    }
   

    /**
     * Set razonSocial
     *
     * @param string $razonSocial
     *
     * @return Administraciones
     */
    public function setRazonSocial($razonSocial)
    {
        $this->razonSocial = $razonSocial;

        return $this;
    }

    /**
     * Get razonSocial
     *
     * @return string
     */
    public function getRazonSocial()
    {
        return $this->razonSocial;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Administraciones
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set domicilio
     *
     * @param string $domicilio
     *
     * @return Administraciones
     */
    public function setDomicilio($domicilio)
    {
        $this->domicilio = $domicilio;

        return $this;
    }

    /**
     * Get domicilio
     *
     * @return string
     */
    public function getDomicilio()
    {
        return $this->domicilio;
    }

    /**
     * Set responsable
     *
     * @param string $responsable
     *
     * @return Administraciones
     */
    public function setResponsable($responsable)
    {
        $this->responsable = $responsable;

        return $this;
    }

    /**
     * Get responsable
     *
     * @return string
     */
    public function getResponsable()
    {
        return $this->responsable;
    }

    /**
     * Set localidadId
     *
     * @param integer $localidadId
     *
     * @return Administraciones
     */
    public function setLocalidadId($localidadId)
    {
        $this->localidadId = $localidadId;

        return $this;
    }

    /**
     * Get localidadId
     *
     * @return integer
     */
    public function getLocalidadId()
    {
        return $this->localidadId;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     *
     * @return Administraciones
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set fchAlta
     *
     * @param \DateTime $fchAlta
     *
     * @return Administraciones
     */
    public function setFchAlta($fchAlta)
    {
        $this->fchAlta = $fchAlta;

        return $this;
    }

    /**
     * Get fchAlta
     *
     * @return \DateTime
     */
    public function getFchAlta()
    {
        return $this->fchAlta;
    }

    /**
     * Set fchModif
     *
     * @param \DateTime $fchModif
     *
     * @return Administraciones
     */
    public function setFchModif($fchModif)
    {
        $this->fchModif = $fchModif;

        return $this;
    }

    /**
     * Get fchModif
     *
     * @return \DateTime
     */
    public function getFchModif()
    {
        return $this->fchModif;
    }

    /**
     * Set usuarioModif
     *
     * @param string $usuarioModif
     *
     * @return Administraciones
     */
    public function setUsuarioModif($usuarioModif)
    {
        $this->usuarioModif = $usuarioModif;

        return $this;
    }

    /**
     * Get usuarioModif
     *
     * @return string
     */
    public function getUsuarioModif()
    {
        return $this->usuarioModif;
    }

    /**
     * Set usuarioAlta
     *
     * @param string $usuarioAlta
     *
     * @return Administraciones
     */
    public function setUsuarioAlta($usuarioAlta)
    {
        $this->usuarioAlta = $usuarioAlta;

        return $this;
    }

    /**
     * Get usuarioAlta
     *
     * @return string
     */
    public function getUsuarioAlta()
    {
        return $this->usuarioAlta;
    }

    /**
     * Set clientId
     *
     * @param string $clientId
     *
     * @return Administraciones
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;

        return $this;
    }

    /**
     * Get clientId
     *
     * @return string
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Set clientSecret
     *
     * @param string $clientSecret
     *
     * @return Administraciones
     */
    public function setClientSecret($clientSecret)
    {
        $this->clientSecret = $clientSecret;

        return $this;
    }

    /**
     * Get clientSecret
     *
     * @return string
     */
    public function getClientSecret()
    {
        return $this->clientSecret;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
}

