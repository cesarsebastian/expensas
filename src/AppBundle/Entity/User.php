<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @Vich\Uploadable
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="isActive", type="boolean")
     */
    private $isActive;
    
    /**
    * Cuit.
    *
    * @var string
    * @ORM\Column(type="string", name="cuit")
    */    
    private $cuit;
    
    /**
    * Dni.
    *
    * @var string
    * @ORM\Column(type="string", name="dni", nullable=true)
    */    
    private $dni;
    
    /**
     * @var \AppBundle\Entity\Administracion     
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Administracion", inversedBy="users" ) 
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="administracion_id", referencedColumnName="id")
     * })
     */
    protected $administracion;

    public function __toString()
    {
        return $this->username;
    }

    public function __construct()
    {
        parent::__construct();
        
        $this->isActive = true;
    }
    
    /**
     * @param Administracion $administracion
     */
    public function setAdministracion($administracion)
    {
        $this->administracion = $administracion;
    }

    /**
     * @return Administracion
     */
    public function getAdministracion()
    {
        return $this->administracion;
    }

    /**
     * Set isActive.
     *
     * @param bool $isActive
     *
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive.
     *
     * @return bool
     */
    public function getIsActive()
    {
        return $this->isActive;
    }
    
    public function getRoles()
    {
        return $this->roles;
    }

    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
        ));
    }

    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->username,
            $this->password) = unserialize($serialized);
    }
    
    public function getCuit() 
    {
        return $this->cuit;
    }

    public function getDni() 
    {
        return $this->dni;
    }
	
    public function getId() 
    {
        return $this->id;
    }	

    public function setCuit($cuit) 
    {
        $this->cuit = $cuit;
    }

    public function setDni($dni) 
    {
        $this->dni = $dni;
    }
}
