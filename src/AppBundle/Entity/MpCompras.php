<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MpCompras
 *
 * @ORM\Table(name="mp_compras")
 * @ORM\Entity
 */
class MpCompras
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=true)
     */
    private $fecha;

    /**
     * @var string
     *
     * @ORM\Column(name="credenciales", type="string", nullable=false)
     */
    private $credenciales;

    /**
     * @var string
     *
     * @ORM\Column(name="descrip", type="string", length=300, nullable=true)
     */
    private $descrip;

    /**
     * @var float
     *
     * @ORM\Column(name="costo", type="float", precision=10, scale=0, nullable=true)
     */
    private $costo;

    /**
     * @var float
     *
     * @ORM\Column(name="totalpago", type="float", precision=10, scale=0, nullable=true)
     */
    private $totalpago;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=10, nullable=true)
     */
    private $estado;

    /**
     * @var string
     *
     * @ORM\Column(name="items", type="text", length=65535, nullable=true)
     */
    private $items;

    /**
     * @var string
     *
     * @ORM\Column(name="logUrl", type="text", length=65535, nullable=true)
     */
    private $logurl;

    /**
     * @var string
     *
     * @ORM\Column(name="logPago", type="text", length=65535, nullable=true)
     */
    private $logpago;

    /**
     * @var int
     *
     * @ORM\Column(name="iduser", type="string", length=10, nullable=true)
     */
    private $iduser;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return MpCompras
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set descrip
     *
     * @param string $descrip
     *
     * @return MpCompras
     */
    public function setDescrip($descrip)
    {
        $this->descrip = $descrip;

        return $this;
    }

    /**
     * Get descrip
     *
     * @return string
     */
    public function getDescrip()
    {
        return $this->descrip;
    }

    /**
     * Set costo
     *
     * @param float $costo
     *
     * @return MpCompras
     */
    public function setCosto($costo)
    {
        $this->costo = $costo;

        return $this;
    }

    /**
     * Get costo
     *
     * @return float
     */
    public function getCosto()
    {
        return $this->costo;
    }

    /**
     * Set iduser
     *
     * @param int $iduser
     *
     * @return MpCompras
     */
    public function setIduser($id)
    {
        $this->iduser = $id;

        return $this;
    }

    /**
     * Get iduser
     *
     * @return int
     */
    public function getIduser()
    {
        return $this->iduser;
    }    

    /**
     * Set creds
     *
     * @param string $credenciales
     *
     * @return MpCompras
     */
    public function setCredenciales($creds)
    {
        $this->credenciales = $creds;

        return $this;
    }

    /**
     * Get credenciales
     *
     * @return string
     */
    public function getCredenciales()
    {
        return $this->credenciales;
    }

    /**
     * Set totalpago
     *
     * @param float $totalpago
     *
     * @return MpCompras
     */
    public function setTotalpago($totalpago)
    {
        $this->totalpago = $totalpago;

        return $this;
    }

    /**
     * Get totalpago
     *
     * @return float
     */
    public function getTotalpago()
    {
        return $this->totalpago;
    }

    /**
     * Set estado
     *
     * @param string $estado
     *
     * @return MpCompras
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set items
     *
     * @param string $items
     *
     * @return MpCompras
     */
    public function setItems($items)
    {
        $this->items = $items;

        return $this;
    }

    /**
     * Get items
     *
     * @return string
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Set logurl
     *
     * @param string $logurl
     *
     * @return MpCompras
     */
    public function setLogurl($logurl)
    {
        $this->logurl = $logurl;

        return $this;
    }

    /**
     * Get logurl
     *
     * @return string
     */
    public function getLogurl()
    {
        return $this->logurl;
    }

    /**
     * Set logpago
     *
     * @param string $logpago
     *
     * @return MpCompras
     */
    public function setLogpago($logpago)
    {
        $this->logpago = $logpago;

        return $this;
    }

    /**
     * Get logpago
     *
     * @return string
     */
    public function getLogpago()
    {
        return $this->logpago;
    }
}
