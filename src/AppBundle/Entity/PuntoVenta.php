<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PuntoVenta
 * @ORM\Table(name="punto_venta")
 * @ORM\Entity
 */
class PuntoVenta
{    
    /**
     * Codigo.
     *
     * @var string
     * @ORM\Column(type="string", name="codigo")
     */    
    private $codigo;

    /**
     * Nombre.
     *
     * @var string
     * @ORM\Column(type="string", name="nombre")
     */    
    private $nombre;

    /**
     * Sec Recibo.
     *
     * @var integer
     * @ORM\Column(type="integer", name="sec_recibo", nullable=true)
     */
    private $secRecibo;

    /**
     * Sec Nota Credito.
     *
     * @var integer
     * @ORM\Column(type="integer", name="sec_notacredito", nullable=true)
     */
    private $secNotacredito;

     /**
     * Sec Factura.
     *
     * @var string
     * @ORM\Column(type="integer", name="sec_factura", nullable=true)
     */
    private $secFactura;

    /**
     * Punto ventacol.
     *
     * @var string
     * @ORM\Column(type="string", name="punto_ventacol", nullable=true)
     */
    private $puntoVentacol;

    /**
     * Active
     *
     * @var bool
     * @ORM\Column(type="boolean", name="active")
     */
    private $active;

    /**
     * Created
     *
     * @var \DateTime
     * @ORM\Column(type="datetime", name="created")
     */
    private $created;

    /**
     * Modified
     *
     * @var \DateTime
     * @ORM\Column(type="datetime", name="modified", nullable=true)
     */
    private $modified;

    /**
     * @var \AppBundle\Entity\Administracion
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Administracion", inversedBy="puntosVentas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="administraciones_id", referencedColumnName="id")
     * })
     */
    private $administracion;    

     /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    
    /**
     * Constructor of the Administracion class.
     * (Initialize some fields).
     */
    public function __construct()
    {   
        $this->created = new \DateTime();        
    }
    
    
    public function __toString()
    {
        return $this->getNombre();
    }


    /**
     * Set codigo
     *
     * @param string $codigo
     *
     * @return PuntoVenta
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return PuntoVenta
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set secRecibo
     *
     * @param integer $secRecibo
     *
     * @return PuntoVenta
     */
    public function setSecRecibo($secRecibo)
    {
        $this->secRecibo = $secRecibo;

        return $this;
    }

    /**
     * Get secRecibo
     *
     * @return integer
     */
    public function getSecRecibo()
    {
        return $this->secRecibo;
    }

    /**
     * Set secNotacredito
     *
     * @param integer $secNotacredito
     *
     * @return PuntoVenta
     */
    public function setSecNotacredito($secNotacredito)
    {
        $this->secNotacredito = $secNotacredito;

        return $this;
    }

    /**
     * Get secNotacredito
     *
     * @return integer
     */
    public function getSecNotacredito()
    {
        return $this->secNotacredito;
    }

    /**
     * Set secFactura
     *
     * @param integer $secFactura
     *
     * @return PuntoVenta
     */
    public function setSecFactura($secFactura)
    {
        $this->secFactura = $secFactura;

        return $this;
    }

    /**
     * Get secFactura
     *
     * @return integer
     */
    public function getSecFactura()
    {
        return $this->secFactura;
    }

    /**
     * Set puntoVentacol
     *
     * @param string $puntoVentacol
     *
     * @return PuntoVenta
     */
    public function setPuntoVentacol($puntoVentacol)
    {
        $this->puntoVentacol = $puntoVentacol;

        return $this;
    }

    /**
     * Get puntoVentacol
     *
     * @return string
     */
    public function getPuntoVentacol()
    {
        return $this->puntoVentacol;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return PuntoVenta
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set Created
     *
     * @param \DateTime $created
     *
     * @return PuntoVenta
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get Created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     *
     * @return PuntoVenta
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * @param Administracion $administracion
     */
    public function setAdministracion($administracion)
    {
        $this->administracion = $administracion;
    }

    /**
     * @return Administracion
     */
    public function getAdministracion()
    {
        return $this->administracion;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}

