<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MotivoAnulacion
 * 
 * @ORM\Table(name="motivo_anulacion")
 * @ORM\Entity
 */

class MotivoAnulacion
{
   /**
    * Descripcion.
    *
    * @var string
    * @ORM\Column(type="string", name="descripcion")
    */
    private $descripcion;

   /**
    * @ORM\Id
    * @ORM\Column(name="id", type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    private $id;
    
    public function __toString()
    {
        return $this->getDescripcion();
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return MotivoAnulacion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}

