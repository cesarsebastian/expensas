<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Comprobantes
 * 
 * @ORM\Table(name="comprobantes")
 * @ORM\Entity
 */

class Comprobante
{
    /**
     * Fecha
     *
     * @var \DateTime
     * @ORM\Column(type="datetime", name="fecha")
     */
    private $fecha;

   /**
    * Prefijo.
    *
    * @var string
    * @ORM\Column(type="string", name="prefijo")
    */
    private $prefijo;

    /**
    * Numero.
    *
    * @var string
    * @ORM\Column(type="string", name="numero")
    */
    private $numero;

    /**
    * Importe.
    *
    * @var string
    * @ORM\Column(type="string", name="importe")
    */
    private $importe;

    /**
    * Detalle.
    *
    * @var string
    * @ORM\Column(type="string", name="detalle", nullable=true)
    */
    private $detalle;

   /**
    * Comprobantecol.
    *
    * @var string
    * @ORM\Column(type="string", name="comprobantecol", nullable=true)
    */
    private $comprobantecol;

    /**
     * Fch Anularion
     *
     * @var \DateTime
     * @ORM\Column(type="datetime", name="fch_anulacion", nullable=true)
     */
    private $fchAnulacion;

   /**
    * Usuario Anulacion.
    *
    * @var string
    * @ORM\Column(type="string", name="usuario_anulacion", nullable=true)
    */
    private $usuarioAnulacion;

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Concepto     
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Concepto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="concepto_id", referencedColumnName="id")
     * })
     */
    private $concepto;

    /**
     * @var \AppBundle\Entity\UnidadFuncional
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\UnidadFuncional", inversedBy="comprobantes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="unidad_id", referencedColumnName="id")
     * })
     */
    private $unidad;

    /**
     * @var \AppBundle\Entity\TipoComprobante
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TipoComprobante")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipo_comprobante_id", referencedColumnName="id")
     * })
     */
    private $tipoComprobante;

    /**
     * @var \AppBundle\Entity\MotivoAnulacion     
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\MotivoAnulacion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="motivo_anulacion_id", referencedColumnName="id")
     * })
     */
    private $motivoAnulacion;
    
    /**
     * @var integer
     * @ORM\Column(name="idpago", type="integer", nullable=true)
     */
    private $idpago;

    /**
     * @var integer
     * @ORM\Column(name="idfactura", type="integer", nullable=true)
     */
    private $idfactura;    

    /**
     * Constructor of the Administracion class.
     * (Initialize some fields).
     */
    public function __construct()
    {   
        $this->fecha = new \DateTime();        
    }    
    
    public function __toString()
    {
        return $this->getNumero();
    }
    
    public function getImporteSigno(){
        return $this->importe * $this->tipoComprobante->getSigno();
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return Comprobantes
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set prefijo
     *
     * @param string $prefijo
     *
     * @return Comprobantes
     */
    public function setPrefijo($prefijo)
    {
        $this->prefijo = $prefijo;

        return $this;
    }

    /**
     * Get prefijo
     *
     * @return string
     */
    public function getPrefijo()
    {
        return $this->prefijo;
    }

    /**
     * Set numero
     *
     * @param string $numero
     *
     * @return Comprobantes
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return string
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set importe
     *
     * @param string $importe
     *
     * @return Comprobantes
     */
    public function setImporte($importe)
    {
        $this->importe = $importe;

        return $this;
    }

    /**
     * Get importe
     *
     * @return string
     */
    public function getImporte()
    {
        return $this->importe;
    }

    /**
     * Set detalle
     *
     * @param string $detalle
     *
     * @return Comprobantes
     */
    public function setDetalle($detalle)
    {
        $this->detalle = $detalle;

        return $this;
    }

    /**
     * Get detalle
     *
     * @return string
     */
    public function getDetalle()
    {
        return $this->detalle;
    }

    /**
     * Set comprobantecol
     *
     * @param string $comprobantecol
     *
     * @return Comprobantes
     */
    public function setComprobantecol($comprobantecol)
    {
        $this->comprobantecol = $comprobantecol;

        return $this;
    }

    /**
     * Get comprobantecol
     *
     * @return string
     */
    public function getComprobantecol()
    {
        return $this->comprobantecol;
    }

    /**
     * Set fchAnularion
     *
     * @param \DateTime $fchAnulacion
     *
     * @return Comprobantes
     */
    public function setFchAnulacion($fchAnulacion)
    {
        $this->fchAnulacion = $fchAnulacion;

        return $this;
    }

    /**
     * Get fchAnulacion
     *
     * @return \DateTime
     */
    public function getFchAnulacion()
    {
        return $this->fchAnulacion;
    }

    /**
     * Set usuarioAnulacion
     *
     * @param string $usuarioAnulacion
     *
     * @return Comprobantes
     */
    public function setUsuarioAnulacion($usuarioAnulacion)
    {
        $this->usuarioAnulacion = $usuarioAnulacion;

        return $this;
    }

    /**
     * Get usuarioAnulacion
     *
     * @return string
     */
    public function getUsuarioAnulacion()
    {
        return $this->usuarioAnulacion;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set concepto
     *
     * @param \AppBundle\Entity\Concepto $concepto
     *
     * @return Comprobantes
     */
    public function setConcepto(\AppBundle\Entity\Concepto $concepto = null)
    {
        $this->concepto = $concepto;

        return $this;
    }

    /**
     * Get concepto
     *
     * @return \AppBundle\Entity\Conceptos
     */
    public function getConcepto()
    {
        return $this->concepto;
    }

    /**
     * Set unidad
     *
     * @param \AppBundle\Entity\UnidadFuncional $unidad
     *
     * @return Comprobantes
     */
    public function setUnidad(\AppBundle\Entity\UnidadFuncional $unidad = null)
    {
        $this->unidad = $unidad;

        return $this;
    }

    /**
     * Get unidad
     *
     * @return \AppBundle\Entity\UnidadFuncional
     */
    public function getUnidad()
    {
        return $this->unidad;
    }

    /**
     * Set tipoComprobante
     *
     * @param \AppBundle\Entity\TipoComprobante $tipoComprobante
     *
     * @return Comprobantes
     */
    public function setTipoComprobante(\AppBundle\Entity\TipoComprobante $tipoComprobante = null)
    {
        $this->tipoComprobante = $tipoComprobante;

        return $this;
    }

    /**
     * Get tipoComprobante
     *
     * @return \AppBundle\Entity\TipoComprobante
     */
    public function getTipoComprobante()
    {
        return $this->tipoComprobante;
    }

    /**
     * Set motivoAnulacion
     *
     * @param \AppBundle\Entity\MotivoAnulacion $motivoAnulacion
     *
     * @return Comprobantes
     */
    public function setMotivoAnulacion(\AppBundle\Entity\MotivoAnulacion $motivoAnulacion = null)
    {
        $this->motivoAnulacion = $motivoAnulacion;

        return $this;
    }

    /**
     * Get motivoAnulacion
     *
     * @return \AppBundle\Entity\MotivoAnulacion
     */
    public function getMotivoAnulacion()
    {
        return $this->motivoAnulacion;
    }

    /**
     * Get idpago
     *
     * @return integer
     */
    public function getIdpago()
    {
        return $this->idpago;
    }

    /**
     * Set idpago
     *
     * @param integer $idpago
     *
     * @return Comprobantes
     */
    public function setIdpago($id)
    {
        $this->idpago = $id;

        return $this;
    }

    /**
     * Get idfactura
     *
     * @return integer
     */
    public function getIdfactura()
    {
        return $this->idfactura;
    }

    /**
     * Set idfactura
     *
     * @param integer $idfactura
     *
     * @return Comprobantes
     */
    public function setIdfactura($id)
    {
        $this->idfactura = $id;

        return $this;
    }    
}

