<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tipo Comprobante
 * @ORM\Table(name="tipo_comprobante")
 * @ORM\Entity
 */
class TipoComprobante
{
    /**
    * Descripcion.
    *
    * @var string
    * @ORM\Column(type="string", name="descripcion")
    */    
    private $descripcion;

    /**
    * Signo.
    *
    * @var string
    * @ORM\Column(type="string", name="signo")
    */    
    private $signo;

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    public function __toString()
    {
        return $this->getDescripcion();
    }


    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return TipoComprobante
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set signo
     *
     * @param string $signo
     *
     * @return TipoComprobante
     */
    public function setSigno($signo)
    {
        $this->signo = $signo;

        return $this;
    }

    /**
     * Get signo
     *
     * @return string
     */
    public function getSigno()
    {
        return $this->signo;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}

