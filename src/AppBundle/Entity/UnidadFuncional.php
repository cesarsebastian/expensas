<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\HttpFoundation\Session\Session;

/**
 * UnidadesFuncionales
 * 
 * @ORM\Table(name="unidades_funcionales")
 * @ORM\Entity
 * @Vich\Uploadable
 */
class UnidadFuncional
{    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="unidades_funcionales_images", fileNameProperty="image")
     * @var File
     */
    private $imageFile;
    
   /**
    * Numero.
    *
    * @var string
    * @ORM\Column(type="string", name="numero", nullable=true)
    * @Assert\NotBlank()
    */
    private $numero;

   /**
    * Propietario.
    *
    * @var string
    * @ORM\Column(type="string", name="propietario", nullable=true)
    * @Assert\NotBlank()
    */
    private $propietario;

   /**
    * Copropietario.
    *
    * @var string
    * @ORM\Column(type="string", name="copropietario", nullable=true)
    * @Assert\NotBlank()
    */
    private $copropietario;

   /**
    * Telefono.
    *
    * @var string
    * @ORM\Column(type="string", name="telefono", nullable=true)
    * @Assert\NotBlank()
    */
    private $telefono;

   /**
    * Celular.
    *
    * @var string
    * @ORM\Column(type="string", name="celular", nullable=true)
    * @Assert\NotBlank()
    */
    private $celular;

   /**
    * Email.
    *
    * @var string
    * @ORM\Column(type="string", name="email")
    * @Assert\NotBlank()
    * @Assert\Email(
    *     message = "The email '{{ value }}' is not a valid email.",
    *     checkMX = true
    * )
    */
    private $email;

    /**
    * Email2.
    *
    * @var string
    * @ORM\Column(type="string", name="email2", nullable=true)
    */
    private $email2;

    /**
    * Direccion.
    *
    * @var string
    * @ORM\Column(type="string", name="direccion")
    */
    private $direccion;
    
    /**
    * Superficie.
    *
    * @var string
    * @ORM\Column(type="string", name="superficie", nullable=true)
     * @Assert\NotBlank()
    */    
    private $superficie;
    
    /**
    * Cuit.
    *
    * @var string
    * @ORM\Column(type="string", name="cuit")
    * @Assert\NotBlank()
    */    
    private $cuit;
    
    /**
    * Dni.
    *
    * @var string
    * @ORM\Column(type="string", name="dni", nullable=true)
    * @Assert\NotBlank()
    */    
    private $dni;

   /**
    * Estado.
    *
    * @var string
    * @ORM\Column(type="string", name="estado", nullable=true)
    */
    private $estado;

   /**
     * @var \AppBundle\Entity\Administracion
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Administracion", inversedBy="unidadesFuncionales")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="administracion_id", referencedColumnName="id")
     * })
     */
    private $administracion;

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\User
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     * })
     */
    private $user;    
    
    /**
     * Users that have been UnidadFuncional.
     *
     * @var Comprobantes[]
     * @ORM\OneToMany(targetEntity="Comprobante", mappedBy="unidad", cascade={"remove"})
     */
    protected $comprobantes;   
    
    public function __construct()
    {
        $this->comprobantes = new ArrayCollection();
        
    }    
    
    public function setComprobantes($comprobantes)
    {
        $this->comprobantes = $comprobantes;
    }

    
    public function getComprobantes()
    {
        return $this->comprobantes;
    }
    
    public function addComprobante(\AppBundle\Entity\Comprobante $comprobante)
    {   
        $this->comprobantes->add($comprobante);
        $comprobante->setUnidad($this);
        
        return $this;
    }
    
    public function removeComprobante(\AppBundle\Entity\Comprobante $comprobante)
    {
        $this->comprobantes->removeElement($comprobante);
        return $this;
    }
    
    public function removeAllComprobantes()
    {
        $this->comprobantes->clear();
    }
    
    public function getSaldo($fecha = null)
    {
        $saldo = 0;
        
        if($fecha == null)
        {
            //$date = '2016-12-31 23:59:00';
            $date = date("Y-m-d").'23:59:00';
        }
        else
        {
            $date = $fecha.' 23:59:00';
        }
        
        $format = 'Y-m-d H:i:s';        
        $query  = \DateTime::createFromFormat($format, $date);
        
        foreach ($this->comprobantes as $c)
        {
            if($c->getFecha()->format('Y-m-d H:i:s') <= $query->format('Y-m-d H:i:s'))
            {
                $saldo+=$c->getImporteSigno();
            }
        }

        return $saldo;
    }

    //Traigo todos los comprobantes de tipo factura activos de esta uf.
    public function getComprobantesFacturas($fecha = null)
    {
        if($fecha == null)
        {
            $date = date("Y-m-d").'23:59:00';
        }
        else
        {
            $date = $fecha.' 23:59:00';
        }
        
        $format = 'Y-m-d H:i:s';        
        $query  = \DateTime::createFromFormat($format, $date);
        $resu   = array();

        //Traigo todas las facturas de la uf que no esten pagadas.
        foreach ($this->comprobantes as $c)
        {
            if($c->getFecha()->format('Y-m-d H:i:s')<=$query->format('Y-m-d H:i:s'))
            {
                if (($c->getTipoComprobante()->getDescripcion()=='Factura')&&($c->getUnidad()->getId()==$this->id))
                {
                    //Que no esten pagadas.
                    if ($c->getIdpago()==null)
                        array_push($resu,$c);
                }
            }
        }

        return $resu;
    }

    //Traigo total deuda
    public function getTotalDeuda($fecha = null)
    {
        if($fecha == null)
        {
            $date = date("Y-m-d").'23:59:00';
        }
        else
        {
            $date = $fecha.' 23:59:00';
        }
        
        $format = 'Y-m-d H:i:s';        
        $query  = \DateTime::createFromFormat($format, $date);
        $resu   = array();
        $total  = 0;

        foreach ($this->comprobantes as $c)
        {
            if($c->getFecha()->format('Y-m-d H:i:s')<=$query->format('Y-m-d H:i:s'))
            {
                if (($c->getTipoComprobante()->getId()==1)&&($c->getUnidad()->getId()==$this->id))
                    $total+=$c->getImporteSigno();
            }
        }

        return $total;
    }


    /**
     * Set numero
     *
     * @param string $numero
     *
     * @return UnidadesFuncionales
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return string
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set propietario
     *
     * @param string $propietario
     *
     * @return UnidadesFuncionales
     */
    public function setPropietario($propietario)
    {
        $this->propietario = $propietario;

        return $this;
    }

    /**
     * Get propietario
     *
     * @return string
     */
    public function getPropietario()
    {
        return $this->propietario;
    }

    /**
     * Set copropietario
     *
     * @param string $copropietario
     *
     * @return UnidadesFuncionales
     */
    public function setCopropietario($copropietario)
    {
        $this->copropietario = $copropietario;

        return $this;
    }

    /**
     * Get copropietario
     *
     * @return string
     */
    public function getCopropietario()
    {
        return $this->copropietario;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     *
     * @return UnidadesFuncionales
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set celular
     *
     * @param string $celular
     *
     * @return UnidadesFuncionales
     */
    public function setCelular($celular)
    {
        $this->celular = $celular;

        return $this;
    }

    /**
     * Get celular
     *
     * @return string
     */
    public function getCelular()
    {
        return $this->celular;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return UnidadesFuncionales
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set email2
     *
     * @param string $email2
     *
     * @return UnidadesFuncionales
     */
    public function setEmail2($email2)
    {
        $this->email2 = $email2;

        return $this;
    }

    /**
     * Get email2
     *
     * @return string
     */
    public function getEmail2()
    {
        return $this->email2;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     *
     * @return UnidadesFuncionales
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set estado
     *
     * @param string $estado
     *
     * @return UnidadesFuncionales
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }
    
    /**
     * Set superficie
     *
     * @param string $superficie
     *
     * @return UnidadesFuncionales
     */
    public function setSuperficie($superficie)
    {
        $this->superficie = $superficie;

        return $this;
    }

    /**
     * Get superficie
     *
     * @return string
     */
    public function getSuperficie()
    {
        return $this->superficie;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set usuario
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return UnidadesFuncionales
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
    
    
    /**
     * Set administracion
     *
     * @param \AppBundle\Entity\Administracion $administracion
     *
     * @return UnidadesFuncionales
     */
    public function setAdministracion(\AppBundle\Entity\Administracion $administracion = null)
    {
        $this->administracion = $administracion;

        return $this;
    }

    /**
     * Get administracion
     *
     * @return \AppBundle\Entity\Administracion
     */
    public function getAdministracion()
    {
        return $this->administracion;
    }
    
    /**
     * @param File $image
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;
    }
    
    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }
    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }
    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }
    
    public function getCuit() 
    {
        return $this->cuit;
    }

    public function getDni() 
    {
        return $this->dni;
    }

    public function setCuit($cuit) 
    {
        $this->cuit = $cuit;
    }

    public function setDni($dni) 
    {
        $this->dni = $dni;
    }
    
    public function __toString()
    {
        return $this->getNumero();
    }
}

