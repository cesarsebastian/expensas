<?php
namespace AppBundle\EventListener;
 
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use JavierEguiluz\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AppSubscriber implements EventSubscriberInterface {

    protected $container;

    /**
     * AppSubscriber constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container) { // this is @service_container 
        $this->container = $container;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents() {
        // return the subscribed events, their methods and priorities
        return array(
            EasyAdminEvents::PRE_NEW => 'checkUserRights',
            EasyAdminEvents::PRE_LIST => 'checkUserRights',
            EasyAdminEvents::PRE_EDIT => 'checkUserRights',
            EasyAdminEvents::PRE_SHOW => 'checkUserRights',
            EasyAdminEvents::PRE_DELETE => 'checkUserRights',
        );
    }

    /**
     * show an error if user is not superadmin and tries to manage restricted st\
      uff
     *
     * @param GenericEvent $event event
     * @return null
     * @throws AccessDeniedException
     */
    public function checkUserRights(GenericEvent $event) {

        // if super admin, allow all
        $authorization = $this->container->get('security.authorization_checker');
        $request = $this->container->get('request_stack')->getCurrentRequest()->query;
        if ($authorization->isGranted('ROLE_SUPER_ADMIN')) {
            return;
        }

        $entity = $request->get('entity');
        $action = $request->get('action');
        $user_id = $request->get('id');

        // This is an exception, allow user to view and edit their own profile irregardless of permissions
        if ($entity == 'User') {
            // if edit and show
            if ($action == 'edit' || $action == 'show') {
                // check user is himself
                if ($user_id == $this->container->get('security.token_storage')->getToken()->getUser()->getId()) {
                    return;
                }
            }
        }

        $config = $this->container->get('easyadmin.config.manager')->getBackendConfig();
        // check for permission for each action
        foreach ($config['entities'] as $k => $v) {
            if ($entity == $k && !$authorization->isGranted($v[$action]['role'])) {
                throw new AccessDeniedException();
            }
        }
    }

}
