<?php
namespace AppBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use AppBundle\Entity\User;
use AppBundle\Entity\Administracion;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AdministracionSuscriber implements EventSubscriberInterface
{
    protected $container;
    protected $em;
    
    function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->em = $container->get('doctrine')->getEntityManager();                       
    }
    
    public static function getSubscribedEvents()
    {
        return array(
            'easy_admin.post_persist' => array('postPersistAdministracion'),             
        );
    }
    
    public function postPersistAdministracion(GenericEvent $event)
    {
        $entity = $event->getSubject();
        
        // only act on some "Administracion" entity
        if (!$entity instanceof Administracion) {
            return;
        }
        
        $encoder = $this->container->get('security.password_encoder');                    
        // 'admin' is the admin user allowed to access the Expensas
        $user = new User();
        $user->setUsername($entity->getEmail());
        $user->setEmail($entity->getEmail());
        $user->setRoles(array('ROLE_ADMIN'));
        $user->setEnabled(true);        
        $user->setPassword($encoder->encodePassword($user, '1234'));
        $user->setCuit('1');
        $user->setDni('1');
        $user->setAdministracion($entity);
        
        $this->em->persist($user);
        $this->em->flush();
    }
    
}
