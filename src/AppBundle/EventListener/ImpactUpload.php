<?php
namespace AppBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use AppBundle\Entity\UnidadFuncionalUpload;
use AppBundle\Entity\UnidadFuncional;
use AppBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Symfony\Component\Validator\Validator\RecursiveValidator as Validator;

class ImpactUpload implements EventSubscriberInterface
{
    protected $container;
    protected $em;
    protected $validator;

    function __construct(ContainerInterface $container, Validator $validator)
    {
        $this->container = $container;
        $this->em = $container->get('doctrine')->getEntityManager();
        $this->validator = $validator;
                
    }
    
    public static function getSubscribedEvents()
    {
        return array(
            'easy_admin.post_persist' => array('impactUpload'),             
        );
    }
    
    public function impactUpload(GenericEvent $event)
    {
        $entity = $event->getSubject();       
        
        // only act on some "UnidadesFuncionalesUpload" entity
        if (!$entity instanceof UnidadFuncionalUpload) {
            return;
        }
        
        $file = $entity->getContractFile();
        
        $errors = array();
        
        if (($handle = fopen($file->getRealPath(), "r")) !== FALSE) {
            $i = 0; //indice de fila
            $k = 0; //cantidad de unidades funcionales dadas de alta correctamente
            
            $administracion = $this->container->get('security.token_storage')->getToken()->getUser()->getAdministracion();
            
            while (($data = fgetcsv($handle, null, ";")) !== FALSE) {
                $i++;
                if (true && $i == 1) { continue; }
                if (count($data) != 11 ) { break; }
                if (!$this->validRow($data)) { break; }
                
                $cuit = $data[2];
                $dni = $data[3];
                $email = $data[7];
                $numero = $data[0];
                
                $user = $this->em->getRepository('AppBundle:User')->findOneBy(array('cuit' => $cuit));
                if(!$user)
                {
                    $encoder = $this->container->get('security.password_encoder');                    
                    // 'admin' is the admin user allowed to access the Expensas
                    $user = new User();
                    $user->setUsername($cuit);
                    $user->setEmail($email);
                    $user->setRoles(array('ROLE_CLIENT'));
                    $user->setEnabled(true);        
                    $user->setPassword($encoder->encodePassword($user, $cuit));
                    $user->setCuit($cuit);
                    $user->setDni($dni);
                    $user->setAdministracion($administracion);
                    $this->em->persist($user);
                    $this->em->flush();
                }

                $uf = $this->em->getRepository('AppBundle:UnidadFuncional')->findOneBy(array('numero' => $numero, 'administracion'=>$administracion->getId()));

                if(!$uf)
                {
                    //Crear usuario
                    //antes de crear pregunto si ya se dio de alta

                    $unidadFuncional = new UnidadFuncional();
                    $unidadFuncional->setNumero($data[0]);
                    $unidadFuncional->setPropietario($data[1]);
                    $unidadFuncional->setCuit($data[2]);
                    $unidadFuncional->setDni($dni);
                    $unidadFuncional->setCopropietario($data[4]);
                    $unidadFuncional->setTelefono($data[5]);
                    $unidadFuncional->setCelular($data[6]);
                    $unidadFuncional->setEmail($email);
                    $unidadFuncional->setEmail2($data[8]);
                    $unidadFuncional->setDireccion($data[9]);
                    $unidadFuncional->setSuperficie($data[10]);
                    $unidadFuncional->setEstado('pendiente');

                    //falta agregar la ad,ministracion que lo toma segun el usuario que esta logeado
                    //setUser
                    $unidadFuncional->setUser($user);

                    //setAdministracion
                    $unidadFuncional->setAdministracion($administracion);
                    $this->em->persist($unidadFuncional);
                    $this->em->flush();

                    $k++;
                }               
            }
            
            $entity->setCantidad($k);
            
            fclose($handle);
        }
        
        $this->em->flush();
    }
    
    private function validRow($data)
    {        
        $valid = true;
        for($j = 0; $j <= 10; $j++) 
        {
            if((!isset($data[$j])) || (empty($data[$j])))
            {
                $valid = false;
                break;
            }
        }
        return $valid;
    }
    
    /**
     * Parse a csv file
     * 
     * @return array
     */
    private function parseCSV()
    {       

        $rows = array();
        if (($handle = fopen($csv->getRealPath(), "r")) !== FALSE) {
            $i = 0;
            while (($data = fgetcsv($handle, null, ";")) !== FALSE) {
                $i++;
                if ($ignoreFirstLine && $i == 1) { continue; }
                $rows[] = $data;
            }
            fclose($handle);
        }

        return $rows;
    }
}
